# People Bingo

Create a people bingo with R.

## Rules

- each person gets a bingo sheet 
- find the people fitting the descriptions
- if you get 5 in a row, you win (other win condition: find all people on sheet)


## How to create your own bingo

- get a csv file with names and infos
- adapt `create_bingo.R` to your csv
- voila!