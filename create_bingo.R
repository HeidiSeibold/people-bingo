## packages
library("ggplot2")

## read data
info0 <- read.csv("people_infos.csv", stringsAsFactors = FALSE)
info <- info0[, c(2, 4)]
names(info) <- c("name", "info")


## preprocess data

### remove weird chars
info$name <- gsub(pattern = "„|“", "", info$name)

### remove emoji
info$info <- gsub(pattern = "😣", replacement = "", info$info)

### Julia won't be able to join
i_julia  <- grep(pattern = "Julia *", info$name)
info <- info[-i_julia, ]

### insert line breaks
info$info_lb <- sapply(strwrap(info$info, 40, simplify = FALSE), 
    paste, collapse = "\n")


## plot
set.seed(1711)
plot_list <- list()

n_bingos <- 40 # number of sheets



for (i in 1:n_bingos) {
    
    ## sample 25 rows from info
    samp <- sample(seq_along(info$name), size = 25, replace = FALSE)
    dat <- cbind(info[samp, ], ind = 1:25)
    
    ## plot
    plot_list[[i]] <- ggplot(dat, aes(x = 1, y = 1)) + 
        geom_text(aes(label = info_lb), size = 2.9) +
        facet_wrap(~ ind) + 
        theme_bw() +
        theme(
            strip.background = element_blank(),
            strip.text.x = element_blank(), 
            panel.grid = element_blank(),
            axis.text = element_blank(),
            axis.title = element_blank(),
            axis.ticks = element_blank()
        )
    
}


pdf(file = "bingo_sheets.pdf", paper = "a4r", width = 12)
sapply(plot_list, print) ## this may take while
dev.off()


